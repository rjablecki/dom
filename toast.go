package dom

import (
	"bitbucket.org/anthracis/tools"
	"fmt"
	"github.com/jroimartin/gocui"
	"strings"
	"time"
)

const (
	kind_error           = "error"
	kind_success         = "success"
	kind_info            = "info"
	toast_timeout        = time.Second * 5
	toast_check_interval = time.Millisecond * 200
)

var gui *gocui.Gui

var toasts []*toast

type toast struct {
	id   string
	msg  string
	kind string
}

func init() {
	toasts = []*toast{}
	go interval()
}

func Error(msg string) *toast {
	return newToast(kind_error, msg, "")
}

func Success(msg string) *toast {
	return newToast(kind_success, msg, "")
}

func Info(msg string) *toast {
	return newToast(kind_info, msg, "")
}

func interval() {
	if len(toasts) > 0 {
		newToasts := []*toast{}
		for _, tst := range toasts {
			_, err := gui.View(tst.id)
			if err == nil {
				newToasts = append(newToasts, tst)
				gui.DeleteView(tst.id)
			}
		}
		toasts = []*toast{}
		for _, tst := range newToasts {
			newToast(tst.kind, tst.msg, tst.id)
		}
	}
	time.Sleep(toast_check_interval)
	interval()
}

func newToast(kind string, msg string, id string) *toast {

	if id == "" {
		id = "toast-" + tools.RandomString(10)
	}
	t := &toast{msg: msg, kind: kind, id: id}
	show := t.render(len(toasts))
	if show {
		toasts = append(toasts, t)
		go func(tid string) {
			time.Sleep(toast_timeout)
			_, err := gui.View(tid)
			if err == nil {
				gui.DeleteView(tid)
				gui.Update(func(g *gocui.Gui) error {
					return nil
				})
			}
		}(t.id)
	}
	return t
}

func (t *toast) render(index int) bool {
	maxX, maxY := gui.Size()
	height := 3
	y1 := maxY - (height * index) - height
	t.msg = strings.TrimSpace(t.msg)
	if y1 > 3 {
		if view, err := gui.SetView(t.id, maxX-len([]rune(t.msg))-3, y1, maxX-2, maxY-(height*index)-1); err != nil {
			view.Wrap = true
			view.Frame = true
			fmt.Fprint(view, t.msg)
			if t.kind == kind_error {
				view.BgColor = gocui.ColorBlack
				view.FgColor = gocui.ColorRed | gocui.AttrBold
			} else if t.kind == kind_info {
				view.BgColor = gocui.ColorBlack
				view.FgColor = gocui.ColorCyan | gocui.AttrBold
			} else {
				view.BgColor = gocui.ColorBlack
				view.FgColor = gocui.ColorGreen | gocui.AttrBold
			}

			return true
		}
	}

	return false
}
