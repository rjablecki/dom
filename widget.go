package dom

import (
	tl "bitbucket.org/anthracis/tools"
	"fmt"
	"github.com/jroimartin/gocui"
	"strings"
	"time"
)

type WidgetInterface interface {
	GetId() string
	SetId(id string) *Widget
	SetContent(text string) *Widget
	GetContent() string
	SetPosition(x, y int) *Widget
	AddContent(text string) *Widget
	SetFrame(frame bool) *Widget
	SetOnTop() *Widget
	Layout(g *gocui.Gui) error
	AddClick(word string, callback func(w *Widget)) *Widget
}

type Widget struct {
	id           string
	body         string
	frame        bool
	x, y, w, h   int
	view         *gocui.View
	BeforeLayout func(w *Widget)
	events       map[string]func(w *Widget)
}

func NewWidget(x, y, w, h int) *Widget {
	id := tl.RandomString(10)
	widget := &Widget{id: id, x: x, y: y, w: w, h: h}
	widget.events = make(map[string]func(w *Widget))

	return widget
}

func (w *Widget) Layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	if w.BeforeLayout != nil {
		w.BeforeLayout(w)
	}

	xPos := w.x
	if xPos < 0 {
		xPos = maxX - 1 + xPos
	}

	yPos := w.y
	if yPos < 0 {
		yPos = maxY - 1 + yPos
	}

	width := xPos + w.w
	if w.w == -1 {
		width = maxX - 1
	}

	height := yPos + w.h
	if w.h == 0 {
		height = maxY - 1
	} else if w.h < 0 {
		height = maxY - 2 + w.h
	}

	v, err := g.SetView(w.id, xPos, yPos, width, height)
	if err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
	}

	v.Clear()
	w.view = v
	v.Frame = w.frame
	fmt.Fprint(v, w.body)

	w.AddEvent(gocui.MouseRelease, gocui.ModNone, w.clickEvent)

	return nil
}

func (w *Widget) Destroy() {
	dom.DeleteView(w.id)
}

func (w *Widget) SetFrame(frame bool) *Widget {
	w.frame = frame

	return w
}

func (w *Widget) SetId(id string) *Widget {
	w.id = id

	return w
}

func (w *Widget) GetId() string {
	return w.id
}

func (w *Widget) Position() (x0, y0, x1, y1 int, err error) {
	x0, y0, x1, y1, err = dom.ViewPosition(w.id)

	return x0, y0, x1, y1, err
}

func (w *Widget) SetPosition(x, y int) *Widget {
	w.x = x
	w.y = y

	return w
}

func (w *Widget) SetContent(text string) *Widget {
	w.body = text

	if w.view != nil {
		w.view.SetOrigin(0, 0)
	}

	return w
}

func (w *Widget) SetColors() *Widget {

	if w.view != nil {
		w.view.BgColor = gocui.ColorGreen
	}

	return w
}

func (w *Widget) AddContent(text string) *Widget {
	w.body = w.body + text

	return w
}

func (w *Widget) SetOnTop() *Widget {

	go (func(wa *Widget) {
		time.Sleep(time.Millisecond * 250)
		dom.SetCurrentView(wa.GetId())
		dom.SetViewOnTop(wa.GetId())
	})(w)

	return w
}

func (w *Widget) GetContent() string {

	return strings.TrimSpace(w.view.Buffer())
}

func (w *Widget) Clear() *Widget {
	w.view.Clear()

	return w
}

func (w *Widget) AddEvent(key interface{}, mod gocui.Modifier, handler func(g *gocui.Gui, view *gocui.View) error) *Widget {
	dom.DeleteKeybindings(w.id)
	dom.SetKeybinding(w.id, key, mod, handler)

	return w
}

func (w *Widget) AddClick(word string, callback func(w *Widget)) *Widget {
	w.events[word] = callback

	return w
}

func (w *Widget) clickEvent(g *gocui.Gui, view *gocui.View) error {
	x, y := view.Cursor()
	word, _ := view.Word(x, y)

	if len(word) > 0 {
		for selector, callback := range w.events {
			if selector == word {
				callback(w)
			}
		}
	}

	return nil
}
