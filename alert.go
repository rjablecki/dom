package dom

import (
	"fmt"
	"github.com/jroimartin/gocui"
	"log"
	"time"
)

type Alert struct {
	*Widget
	question    string
	enabled     bool
	callbackYes func(b *Alert)
}

func NewAlert(question string) *Alert {

	widget := NewWidget(1, 1, 40, 8)
	widget.id = "Alert-" + widget.id

	Alert := &Alert{Widget: widget, question: question}
	Alert.enabled = true
	Alert.frame = true

	return Alert
}

func (l *Alert) Layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	l.x = (maxX - l.w) / 2
	l.y = (maxY - l.h) / 2

	l.Widget.Layout(g)
	l.view.Wrap = true
	l.view.Frame = false
	l.view.BgColor = gocui.ColorWhite
	l.view.FgColor = gocui.ColorBlack

	skip := len(l.question)/l.w - 1
	body := "\n"
	for i := 0; i < l.h; i++ {
		if i == 0 {
			body += "\n"
		} else if i == 1 {
			body += l.question
		} else if i == l.h-2 {
			body += fmt.Sprintf("\033[3%d;%dm OK \033[0m", 2, 7)
		} else {
			if i > skip {
				body += "\n"
			}
		}
	}
	l.Clear()
	fmt.Fprintf(l.view, body)

	l.AddEvent(gocui.MouseRelease, gocui.ModNone, l.clickEvent)

	return nil
}

func (l *Alert) ClickYes(callback func(l *Alert)) *Alert {
	l.callbackYes = callback

	return l
}

func (l *Alert) clickEvent(g *gocui.Gui, view *gocui.View) error {

	x, y := view.Cursor()
	word, _ := view.Word(x, y)

	if l.enabled {
		if word == "OK" {
			if l.callbackYes != nil {
				log.Println("trigger callback")
				l.callbackYes(l)
			}
			l.enabled = false
			go l.returnState()
		}
	}

	return nil
}

func (l *Alert) returnState() *Alert {
	time.Sleep(250 * time.Millisecond)
	l.enabled = true

	Refresh()

	return l
}
