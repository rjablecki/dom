package dom

import (
	"github.com/jroimartin/gocui"
	"strings"
	"time"
)

type Prompt struct {
	*Widget
	question string
	enabled  bool
	callback func(b *Prompt)
}

func NewPrompt(question string) *Prompt {
	widget := NewWidget(1, 1, 50, 4)
	widget.id = "prompt-" + widget.id

	confirm := &Prompt{Widget: widget, question: question}
	confirm.enabled = true
	confirm.frame = true

	return confirm
}

func (l *Prompt) Layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	l.x = (maxX - l.w) / 2
	l.y = (maxY - l.h) / 2
	if l.view != nil {
		l.body = l.view.Buffer()
	}
	l.Widget.Layout(g)
	l.view.Wrap = true
	l.view.Frame = true
	l.view.Editable = true
	l.view.Title = l.question

	l.AddEvent(gocui.MouseRelease, gocui.ModNone, l.clickEvent)
	dom.SetKeybinding(l.GetId(), gocui.KeyEnter, gocui.ModNone, l.keyEnter)

	return nil
}

func (l *Prompt) Callback(callback func(l *Prompt)) *Prompt {
	l.callback = callback

	return l
}

func (l *Prompt) keyEnter(g *gocui.Gui, view *gocui.View) error {
	if l.callback != nil {
		l.callback(l)
	}

	return nil
}

func (l *Prompt) clickEvent(g *gocui.Gui, view *gocui.View) error {
	g.SetCurrentView(l.GetId())
	view.Rewind()

	go func(vv *gocui.View) {
		time.Sleep(time.Millisecond * 200)
		text := strings.TrimSpace(vv.ViewBuffer())
		vv.SetCursor(len(text), 0)

	}(view)

	return nil
}
