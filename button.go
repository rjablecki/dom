package dom

import (
	"fmt"
	"github.com/jroimartin/gocui"
	"strings"
	"time"
)

const (
	buttonHeight       = 2
	defaultWidth       = 20
	defaultDisableTime = 250 * time.Millisecond
)

type Button struct {
	*Widget
	disableTime time.Duration
	enabled     bool
	callback    func(b *Button)
}

func NewButton(label string, x, y int) *Button {
	widget := NewWidget(x, y, defaultWidth, buttonHeight)
	widget.id = "button-" + widget.id

	button := &Button{Widget: widget}
	button.body = label
	button.disableTime = defaultDisableTime
	button.enabled = true
	button.frame = true

	return button
}

func (b *Button) Layout(g *gocui.Gui) error {

	b.Widget.Layout(g)

	var padding string
	paddingSize := (b.w - len([]rune(b.body))) / 2

	if paddingSize > 0 {
		padding = strings.Repeat(" ", paddingSize)
	}

	b.Clear()
	fmt.Fprintf(b.view, padding+b.body)

	b.AddEvent(gocui.MouseRelease, gocui.ModNone, b.clickEvent)

	if b.enabled {
		b.view.FgColor = gocui.ColorYellow | gocui.AttrBold
	} else {
		b.view.FgColor = gocui.ColorGreen | gocui.AttrBold
	}

	return nil
}

func (b *Button) Click(callback func(b *Button)) *Button {
	b.callback = callback

	return b
}

func (b *Button) clickEvent(g *gocui.Gui, view *gocui.View) error {

	if b.enabled {
		b.enabled = false
		go b.returnState(b.view)

		if b.callback != nil {
			b.callback(b)
		}
	}

	return nil
}

func (b *Button) returnState(v *gocui.View) *Button {
	time.Sleep(b.disableTime)
	b.enabled = true

	Refresh()

	return b
}
