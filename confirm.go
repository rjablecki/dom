package dom

import (
	"fmt"
	"github.com/jroimartin/gocui"
	"log"
	"time"
)

type Confirm struct {
	*Widget
	question    string
	enabled     bool
	callbackYes func(b *Confirm)
	callbackNo  func(b *Confirm)
}

func NewConfirm(question string) *Confirm {

	widget := NewWidget(1, 1, 40, 8)
	widget.id = "confirm-" + widget.id

	confirm := &Confirm{Widget: widget, question: question}
	confirm.enabled = true
	confirm.frame = true

	return confirm
}

func (l *Confirm) Layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	l.x = (maxX - l.w) / 2
	l.y = (maxY - l.h) / 2

	l.Widget.Layout(g)
	l.view.Wrap = true
	l.view.Frame = false
	l.view.BgColor = gocui.ColorWhite
	l.view.FgColor = gocui.ColorBlack

	skip := len(l.question)/l.w - 1
	body := "\n"
	for i := 0; i < l.h; i++ {
		if i == 0 {
			body += "\n"
		} else if i == 1 {
			body += l.question
		} else if i == l.h-2 {
			body += fmt.Sprintf("          \033[3%d;%dm TAK \033[0m         \033[3%d;%dm NIE \033[0m", 2, 7, 1, 7)
		} else {
			if i > skip {
				body += "\n"
			}
		}
	}
	l.Clear()
	fmt.Fprintf(l.view, body)

	l.AddEvent(gocui.MouseRelease, gocui.ModNone, l.clickEvent)

	return nil
}

func (l *Confirm) ClickYes(callback func(l *Confirm)) *Confirm {
	l.callbackYes = callback

	return l
}

func (l *Confirm) ClickNo(callback func(l *Confirm)) *Confirm {
	l.callbackNo = callback

	return l
}

func (l *Confirm) clickEvent(g *gocui.Gui, view *gocui.View) error {

	x, y := view.Cursor()
	word, _ := view.Word(x, y)

	if l.enabled {
		if word == "TAK" {
			log.Println("click TAK")
			if l.callbackYes != nil {
				log.Println("trigger callback")
				l.callbackYes(l)
			}
			l.enabled = false
			go l.returnState()
		} else if word == "NIE" {
			log.Println("click NIE")
			if l.callbackNo != nil {
				log.Println("trigger callback")
				l.callbackNo(l)
			}
			l.enabled = false
			go l.returnState()
		}
	}

	return nil
}

func (l *Confirm) returnState() *Confirm {
	time.Sleep(250 * time.Millisecond)
	l.enabled = true

	Refresh()

	return l
}
