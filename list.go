package dom

import (
	tl "bitbucket.org/anthracis/tools"
	"fmt"
	"github.com/jroimartin/gocui"
	"log"
	"strings"
)

type List struct {
	*Panel
	data                     *tl.Options
	selected, selectedColour string
	callback, callbackRight  func(list *List)
}

const (
	PREV_LABEL = "prev"
	NEXT_LABEL = "next"
)

func NewList(options map[string]string, x, y, w, h int) *List {
	panel := NewPanel(x, y, w, h)
	l := &List{Panel: panel}
	l.data = tl.NewOptions(options)
	l.selectedColour = tl.GREEN

	return l
}

func (l *List) SetSelected(selected string) *List {
	l.selected = selected
	return l
}

func (l *List) SetCallback(callback func(list *List)) *List {
	l.callback = callback
	return l
}

func (l *List) SetCallbackRight(callback func(list *List)) *List {
	l.callbackRight = callback
	return l
}

func (l *List) SetOptions(options map[string]string) *List {
	l.data = tl.NewOptions(options)
	go l.refreshContent()

	return l
}

func (l *List) Reset() *List {
	l.selected = ""
	l.refreshContent()
	return l
}

func (l *List) GetValue() string {
	return l.selected
}

func (l *List) Render() *List {
	if v, err := dom.SetView(l.id, l.x, l.y, l.x+l.w, l.y+l.h); err != nil {
		v.Frame = true
		v.FgColor = gocui.ColorWhite | gocui.AttrBold
		v.Title = l.title
		dom.SetKeybinding(l.id, gocui.MouseLeft, gocui.ModNone, l.clickListLeft)
		dom.SetKeybinding(l.id, gocui.MouseRight, gocui.ModNone, l.clickListRight)
		l.refreshContent()
	}
	return l
}

func (l *List) refreshContent() *List {
	v, err := dom.View(l.id)
	if err != nil {
		log.Print(err.Error())
		return l
	}
	v.Clear()

	y := 0
	for _, item := range l.data.LabelsChunkLimit(l.limit, l.page-1) {
		value, _ := l.data.GetValueByLabel(item)
		if l.selected == value {
			item = tl.Color(item, l.selectedColour)
		}

		fmt.Fprintln(v, item)
		y++
	}
	l.drawPagination(y)

	Refresh()

	return l
}

func (l *List) drawPagination(y int) {

	v, err := dom.View(l.id)
	if err != nil {
		log.Print(err.Error())
		return
	}
	maxPage := l.data.GetMaxPage(l.limit)
	if maxPage > 1 {
		for y < l.limit {
			fmt.Fprintln(v, "")
			y++
		}
		pageInfo := fmt.Sprintf("%d/%d", l.page, maxPage)
		pageInfoWidth := len(pageInfo)
		pagination := fmt.Sprintf("\x1b[1;32;40m%s %s\x1b[0m", PREV_LABEL, NEXT_LABEL)
		paginationWidth := len(PREV_LABEL + " " + NEXT_LABEL)
		paddingWidth := l.w - (pageInfoWidth + paginationWidth) - 2

		fmt.Fprintln(v, strings.Repeat("─", l.w))
		fmt.Fprintln(v, fmt.Sprintf("%s%s\x1b[1;33;40m%s\x1b[0m", pagination, strings.Repeat(" ", paddingWidth), pageInfo))
	}
}

func (l *List) clickListLeft(g *gocui.Gui, v *gocui.View) error {

	l.clickList(g, v, gocui.MouseLeft)

	return nil
}

func (l *List) clickListRight(g *gocui.Gui, v *gocui.View) error {

	l.clickList(g, v, gocui.MouseRight)

	return nil
}

func (l *List) clickList(g *gocui.Gui, v *gocui.View, key gocui.Key) error {
	x, y := v.Cursor()
	line, err := v.Line(y)

	if err != nil || len(line) == 0 {
		return nil
	}

	line = strings.TrimSpace(line)
	if y > l.limit {
		word, _ := v.Word(x, y)
		maxPage := l.data.GetMaxPage(l.limit)
		if word == PREV_LABEL && l.page > 1 {
			l.page--
		} else if word == NEXT_LABEL && l.page < maxPage {
			l.page++
		}
	} else {
		value, err := l.data.GetValueByLabel(line)
		if err == nil {
			l.selected = value

			if key == gocui.MouseLeft && l.callback != nil {
				l.callback(l)
			}

			if key == gocui.MouseRight && l.callbackRight != nil {
				l.callbackRight(l)
			}
		}
	}

	l.refreshContent()

	return nil
}
