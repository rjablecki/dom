package dom

import (
	"errors"
	"fmt"
	"github.com/jroimartin/gocui"
	"log"
)

var dom *gocui.Gui
var widgets []WidgetInterface

func Init(cursor, mouse bool) {

	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		panic(err)
	}

	g.Cursor = cursor
	g.Mouse = mouse
	dom = g
}

func Loop() {

	if err := dom.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func GetGui() *gocui.Gui {

	return dom
}

func AddWidget(newWidgets ...WidgetInterface) error {
	for _, widget := range newWidgets {
		id := widget.GetId()
		if WidgetExists(id) {
			return errors.New(fmt.Sprintf("Widget with id '%s' already exists", id))
		}
	}

	widgets = append(widgets, newWidgets...)
	Redraw()
	for _, widget := range newWidgets {
		dom.SetCurrentView(widget.GetId())
		dom.SetViewOnTop(widget.GetId())
	}

	return nil
}

func WidgetExists(id string) bool {
	for _, widget := range widgets {
		if widget.GetId() == id {
			return true
		}
	}

	return false
}

func GetWidget(id string) (WidgetInterface, error) {
	for _, widget := range widgets {
		if widget.GetId() == id {
			return widget, nil
		}
	}

	return nil, gocui.ErrUnknownView
}

func RemoveWidget(id string) {
	var newWidgets []WidgetInterface
	log.Println("RemoveWidget: " + id)
	for _, widget := range widgets {
		if widget.GetId() == id {
			continue
		}
		newWidgets = append(newWidgets, widget)
	}
	widgets = newWidgets

	Redraw()
}

func Redraw() {
	dom.SetManager(getManagers()...)
}

func Refresh() {
	dom.Update(func(g *gocui.Gui) error {
		return nil
	})
}

func getManagers() []gocui.Manager {
	var managers []gocui.Manager
	for _, widget := range widgets {
		managers = append(managers, widget)
	}

	return managers
}
