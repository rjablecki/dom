package dom

import (
	"github.com/jroimartin/gocui"
)

type Panel struct {
	*Widget
	title       string
	page, limit int
}

func NewPanel(x, y, w, h int) *Panel {
	widget := NewWidget(x, y, w, h)
	p := &Panel{Widget: widget}
	p.page = 1
	p.limit = h - 3
	p.frame = true

	return p
}

func (l *Panel) Layout(g *gocui.Gui) error {
	l.Widget.Layout(g)
	l.view.Wrap = true
	l.view.Autoscroll = true

	return nil
}

func (l *Panel) SetTitle(title string) *Panel {
	l.title = title

	return l
}
